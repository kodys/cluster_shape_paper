# README #

The repository contains the latex source for a planned paper on cluster shape based position reconstruction 
and it's application to the vertex detector (VXD) of the Belle II experiment. 

On a linux box, you can compile the document by typing

```
$ pdflatex  2017_ShapeCorrections_Paper.tex
```

contact: benjamin.schwenker@phys.uni-goettingen.de